# Analog Red ConnectIQ Watchface
Based on Active analog (https://gitlab.com/snaggen/ConnectIQ-ActiveAnalog) source code I have tweaked to add seconds and change colors.
Analog red aims to be a clean and simple analog watchface, that provides activity information and progress towards your goals using activity circles.

The following status notifications are supported:
* Notifications
* Alarms
* Do not disturb
* Low battery 
* No phone connection

The watchface is released under the GPLv3 license.

