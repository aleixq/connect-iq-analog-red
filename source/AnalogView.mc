//
// Copyright 2016-2017 by Garmin Ltd. or its subsidiaries.
// Subject to Garmin SDK License Agreement and Wearables
// Application Developer Agreement.
//

using Toybox.Graphics;
using Toybox.Lang;
using Toybox.Math;
using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.WatchUi;
using Toybox.Application;
using Toybox.SensorHistory;

var partialUpdatesAllowed = false;

// This implements an analog watch face
// Original design by Austen Harbour
class AnalogView extends WatchUi.WatchFace
{
    var tinyFont;
    var iconsFont;
    // TODO: USE font as icons
    var isAwake;
    var screenShape;
    var dndIcon;
    var lowBatteryIcon;
    var noBatteryIcon;
    var threequartersBatteryIcon;
    var midBatteryIcon;
    var fullBatteryIcon;
    var noPhoneIcon;
    var alarmIcon;
    var alarmCleanIcon;
    var notificationIcon;
    var bluetoothIcon;
    var stepsIcon;
    var floorIcon;
    var activeIcon;
    var offscreenBuffer;
    var dateBuffer;
    var curClip;
    var screenCenterPoint;
    var fullScreenRefresh;
    
    // Initialize variables for this view
    function initialize() {
        WatchFace.initialize();
        screenShape = System.getDeviceSettings().screenShape;
        fullScreenRefresh = true;        
        partialUpdatesAllowed = ( Toybox.WatchUi.WatchFace has :onPartialUpdate );
    }

    // Configure the layout of the watchface for this device
    function onLayout(dc) {

        // Load the custom font
        tinyFont = WatchUi.loadResource(Rez.Fonts.NormalFont);
        iconsFont = WatchUi.loadResource(Rez.Fonts.IconsFont);

        // If this device supports the Do Not Disturb feature,
        // load the associated Icon into memory.
        if (System.getDeviceSettings() has :doNotDisturb) {
            dndIcon = WatchUi.loadResource(Rez.Drawables.DoNotDisturbIcon);
        } else {
            dndIcon = null;
        }

        if (System.getDeviceSettings() has :alarmCount) {
            alarmIcon = WatchUi.loadResource(Rez.Drawables.AlarmIcon);
            alarmCleanIcon = WatchUi.loadResource(Rez.Drawables.AlarmCleanIcon);
        } else {
            alarmIcon = null;
            alarmCleanIcon = null;
        }

        if (System.getDeviceSettings() has :notificationCount) {
            notificationIcon = WatchUi.loadResource(Rez.Drawables.NotificationIcon);
        } else {
            notificationIcon = null;
        }
        
        // BLUETOOTH ICON NOT USED
        if (System.getDeviceSettings() has :phoneConnected) {
            bluetoothIcon = WatchUi.loadResource(Rez.Drawables.BluetoothIcon);
        } else {
            bluetoothIcon = null;
        }
        
        if (System.getSystemStats() has :battery) {
            lowBatteryIcon = WatchUi.loadResource(Rez.Drawables.LowBatteryIcon);
            noBatteryIcon = WatchUi.loadResource(Rez.Drawables.NoBatteryIcon);
            midBatteryIcon = WatchUi.loadResource(Rez.Drawables.MidBatteryIcon);
            threequartersBatteryIcon = WatchUi.loadResource(Rez.Drawables.ThreeQuartersBatteryIcon);
            fullBatteryIcon = WatchUi.loadResource(Rez.Drawables.FullBatteryIcon);
        } else {
            lowBatteryIcon = null;
            noBatteryIcon = null;
        }

        if (System.getDeviceSettings() has :phoneConnected) {
            noPhoneIcon = WatchUi.loadResource(Rez.Drawables.NoPhoneIcon);
        } else {
            noPhoneIcon = null;
        }

        if (ActivityMonitor.getInfo() has :steps) {
            stepsIcon = WatchUi.loadResource(Rez.Drawables.StepIcon);
        } else {
            stepsIcon = null;
        }

        if (ActivityMonitor.getInfo() has :floorsClimbed) {
            floorIcon = WatchUi.loadResource(Rez.Drawables.FloorIcon);
        } else {
            floorIcon = null;
        }

        if (ActivityMonitor.getInfo() has :activeMinutesWeek) {
            activeIcon = WatchUi.loadResource(Rez.Drawables.ActiveIcon);
        } else {
            activeIcon = null;
        }

        // If this device supports BufferedBitmap, allocate the buffers we use for drawing
        if(Toybox.Graphics has :BufferedBitmap) {
            // Allocate a full screen size buffer with a palette of only 4 colors to draw
            // the background image of the watchface.  This is used to facilitate blanking
            // the second hand during partial updates of the display
            offscreenBuffer = new Graphics.BufferedBitmap({
                :width=>dc.getWidth(),
                :height=>dc.getHeight()
            });

            // Allocate a buffer tall enough to draw the date into the full width of the
            // screen. This buffer is also used for blanking the second hand. This full
            // color buffer is needed because anti-aliased fonts cannot be drawn into
            // a buffer with a reduced color palette
            dateBuffer = new Graphics.BufferedBitmap({
                :width=>dc.getWidth(),
                :height=>Graphics.getFontHeight(Graphics.FONT_MEDIUM)
            });
        } else {
            offscreenBuffer = null;
        }

        curClip = null;

        screenCenterPoint = [dc.getWidth()/2, dc.getHeight()/2];
    }
    

    // This function is used to generate the coordinates of the 4 corners of the polygon
    // used to draw a watch hand. The coordinates are generated with specified length,
    // tail length, and width and rotated around the center point at the provided angle.
    // 0 degrees is at the 12 o'clock position, and increases in the clockwise direction.
    function generateHandCoordinates(centerPoint, angle, handLength, tailLength, width) {
        // Map out the coordinates of the watch hand
        var coords = [[-(width / 2), tailLength], [-(width / 2), -handLength], [width / 2, -handLength], [width / 2, tailLength]];
        var result = new [4];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < 4; i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + 0.5;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + 0.5;

            result[i] = [centerPoint[0] + x, centerPoint[1] + y];
        }

        return result;
    }

	function drawHand(dc, center, angle, length, tailLength, width, color)
    	{
        var coords = [[0,0 + tailLength],  [-(width)*1/2, 0  + tailLength], [-(width)*1/2, -(length)*9/10],[0, -length], [(width)*1/2, -(length)*9/10],[(width)*1/2, 0 + tailLength],[0,0 + tailLength],  [0, -(length)*2/5],[(width)*3/8, -(length)*2/5], [(width)*3/8, -(length)*35/40], [0, -(length)*19/20],[-(width)*3/8, -(length)*35/40], [-(width)*3/8, -(length)*2/5], [0, -(length)*2/5]];
        var result = new [coords.size()];
		var centerX = center[0];
		var centerY = center[1];
		var cos = Math.cos(angle);
		var sin = Math.sin(angle);

		for (var i = 0; i < coords.size(); i += 1)
        {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin);
            var y = (coords[i][0] * sin) + (coords[i][1] * cos);
            result[i] = [ centerX+x, centerY+y];
         
            
        }
        // OUTLINE DRAW
            	dc.setPenWidth(4);
            	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);        
        for (var i = 0; i < result.size(); i += 1){
            if (i < result.size()-1){
            	dc.drawLine(result[i][0], result[i][1], result[i+1][0], result[i+1][1]);
            }           
        }
        // FILL 
        dc.setColor(color, Graphics.COLOR_WHITE);
        dc.fillPolygon(result);         
    	}    
    	
    function drawHands(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();
        var clockTime = System.getClockTime();
        var minuteHandAngle;
        var hourHandAngle;
        var thickness = 12;
        var tailLength = 8;
        var minuteHandLength = 110;
        var hourHandLength = 75;
        // Draw the hour hand. Convert it to minutes and compute the angle.
        hourHandAngle = (((clockTime.hour % 12) * 60) + clockTime.min);
        hourHandAngle = hourHandAngle / (12 * 60.0);
        hourHandAngle = hourHandAngle * Math.PI * 2;
        drawHand(dc, screenCenterPoint, hourHandAngle, hourHandLength, tailLength, thickness, Graphics.COLOR_WHITE);

        //dc.fillPolygon(generateHandCoordinates(screenCenterPoint, hourHandAngle, 65, 10, 5));

        // Draw the minute hand.
        minuteHandAngle = (clockTime.min / 60.0) * Math.PI * 2;
        drawHand(dc, screenCenterPoint, minuteHandAngle, minuteHandLength, tailLength, thickness, Graphics.COLOR_WHITE);
        //dc.fillPolygon(generateHandCoordinates(screenCenterPoint, minuteHandAngle, 90, 10, 5));

        // Draw the arbor in the center of the screen.
        //dc.setPenWidth(1);
        //dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        //dc.fillCircle(screenCenterPoint[0], screenCenterPoint[1], Math.floor(thickness/2));
        //dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_BLACK);
        //dc.drawCircle(screenCenterPoint[0], screenCenterPoint[1], Math.floor(thickness/2)+1);
    }

    function drawTopField(dc) {
    	var app = Application.getApp();
	    var width = dc.getWidth();
	    var height = dc.getHeight();    	
	    var topStr = "";
	    dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
    	if ( app.getProperty("topField")==0 ){
    		// Field fills with Date
	        var info = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
	        topStr = Lang.format("$1$ $2$", [info.day_of_week, info.day]);

	     } else if(app.getProperty("topField")==1 && ActivityMonitor has :getHeartRateHistory){
	     	// Field fills with HRM
	     	topStr = ActivityMonitor.getHeartRateHistory(1, true).next().heartRate.toString() + "BPM";
	     } else if(app.getProperty("topField")==2 && Toybox has :SensorHistory && Toybox.SensorHistory has :getPressureHistory){
	     	// Field fills with Pressure
	     	var pressure = Toybox.SensorHistory.getPressureHistory({}).next().data.toNumber() * 0.01;
	     	topStr = pressure.toNumber() + "hPa";
	     } else if(app.getProperty("topField")==3 && Toybox has :SensorHistory && (Toybox.SensorHistory has :getTemperatureHistory)){
	     	// Field fills with Temperature
	     	topStr = Toybox.SensorHistory.getTemperatureHistory({}).next().data.toNumber() + "º";
	     }
	     dc.drawText(width/2, height/2 - 50, Graphics.FONT_XTINY, topStr, Graphics.TEXT_JUSTIFY_CENTER);
    }
    
    function drawBattery(dc, drawStart, vPos) {
        var width = dc.getWidth();
        var height = dc.getHeight();  
        //var drawStart = width / 2 + 30;  
        //var vPos = 1*height/4;
        if (null != noBatteryIcon && System.getSystemStats().battery <= 5.0) {
            dc.drawBitmap( drawStart, vPos, noBatteryIcon);
        } else if (null != lowBatteryIcon && System.getSystemStats().battery <= 25.0) {
            dc.drawBitmap( drawStart, vPos, lowBatteryIcon);
        } else if (null != lowBatteryIcon && System.getSystemStats().battery <= 50.0) {
            dc.drawBitmap( drawStart, vPos, midBatteryIcon);
        } else if (null != lowBatteryIcon && System.getSystemStats().battery <= 75.0) {
            dc.drawBitmap( drawStart, vPos, threequartersBatteryIcon);
        } else if (null != lowBatteryIcon && System.getSystemStats().battery <= 100.0) {
            dc.drawBitmap( drawStart, vPos, fullBatteryIcon);
        }   
        //drawStart += 40;
        // Draw the battery percentage directly to the main screen.
        var dataString = (System.getSystemStats().battery + 0.5).toNumber().toString() + "%";
        dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
        dc.drawText(drawStart+10, vPos+10, tinyFont, dataString, Graphics.TEXT_JUSTIFY_CENTER);    
        
 		// Show bluetooth icon
		//if (System.getDeviceSettings().phoneConnected) { 
		//	dc.drawBitmap(width / 2 - 60, height/2-60, bluetoothIcon); 
		//}        
    }
    
    function drawStatusBar(dc) {
    	var app = Application.getApp();
        var padding = 8;
        var width = dc.getWidth();
        var height = dc.getHeight();
        // Draw the do-not-disturb icon if we support it and the setting is enabled
        
        var numberOfIcons = 0;
        if (null != dndIcon && System.getDeviceSettings().doNotDisturb && app.getProperty("showDoNotDisturb")) {
           numberOfIcons++;
        }
        if (null != notificationIcon && System.getDeviceSettings().notificationCount && app.getProperty("showNotificationCount")) {
            numberOfIcons++;
        }
        if (null != alarmIcon && System.getDeviceSettings().alarmCount && app.getProperty("showAlarmCount")) {
            numberOfIcons++;
        }
        if (null != lowBatteryIcon && System.getSystemStats().battery <= 100.0 && app.getProperty("showBattery")) {
            numberOfIcons++;
        }
        if (null != noPhoneIcon && !System.getDeviceSettings().phoneConnected && app.getProperty("showNoPhone")) {
            numberOfIcons++;
        }
        if (numberOfIcons == 0) {
            return;
        }

        var drawStart = width/2 - (numberOfIcons*(22+padding)/2);
        var vPos = height / 4 - 25;
        if (null != dndIcon && System.getDeviceSettings().doNotDisturb && app.getProperty("showDoNotDisturb")) {
            dc.drawBitmap( drawStart, vPos, dndIcon);
            drawStart += 22 + padding;
        }

        var notificationCount = System.getDeviceSettings().notificationCount;
        if (null != notificationIcon && notificationCount && app.getProperty("showNotificationCount")) {
            dc.drawBitmap( drawStart, vPos, notificationIcon);
            if (notificationCount > 1 && notificationCount <= 99) {
                dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
                dc.drawText(drawStart+12, vPos-5, tinyFont, notificationCount, Graphics.TEXT_JUSTIFY_CENTER);
            }
            drawStart += 22 + padding;
        }

        var alarmCount = System.getDeviceSettings().alarmCount;
        if (null != alarmIcon && alarmCount && app.getProperty("showAlarmCount")) {
            if (alarmCount > 1 && alarmCount <= 99) {
                dc.drawBitmap( drawStart, vPos, alarmCleanIcon);
                dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
                dc.drawText(drawStart+11, vPos-3, tinyFont, alarmCount, Graphics.TEXT_JUSTIFY_CENTER);
            } else {
                dc.drawBitmap( drawStart, vPos, alarmIcon);
            }
            drawStart += 22 + padding;
        }
		
		if (null != lowBatteryIcon && System.getSystemStats().battery <= 100.0 && app.getProperty("showBattery")) {
			drawBattery(dc, drawStart, vPos-4);
			drawStart += 22 + padding;
		}
			
        if (null != noPhoneIcon && !System.getDeviceSettings().phoneConnected  && app.getProperty("showNoPhone")) {
            dc.drawBitmap( drawStart, vPos, noPhoneIcon);
            drawStart += 24 + padding;
        }
    }

    
    function drawActivityCircles(dc) {
		var app = Application.getApp();
        var width = dc.getWidth();
        var height = dc.getHeight();
        var handTailSize = 8; //TODO: Fix this
        var largeTickSize = 10;
        var padding = 3;
        var stepOuterRadius = Math.floor(((height/2) - handTailSize - largeTickSize - (2*padding))/2);
        var stepInnerRadius = stepOuterRadius - 5;
        var sideOuterRadius = stepOuterRadius - 10;
        var sideInnerRadius = sideOuterRadius - 5;
        var vCenter = height/2 + handTailSize + stepOuterRadius;
        var r = vCenter - height/2;
        
        


        /* Active minutes circle */
        /* NExt active block can be used as starting point of allowing new datafields, the floor and steps must be rearranged from this */
        if (activeIcon != null) {
            var leftAngle = (260.0 / 360) * Math.PI * 2;
            var lX = (width/2 + r * Math.sin(leftAngle)).toLong();
            var lY = (height/2 - r * Math.cos(leftAngle)).toLong();
            dc.setPenWidth(6);
            dc.setColor(app.getProperty("accentBackgroundColor"),Graphics.COLOR_BLACK);
            dc.drawArc(lX, lY,sideOuterRadius,Graphics.ARC_COUNTER_CLOCKWISE, 354, 286);
            
            
            // To-do, add more data fields here.
            //if (app.getProperty("leftField") == 0){
            	var dataGoal = ActivityMonitor.getInfo().activeMinutesWeekGoal;
            	var dataCurrent = ActivityMonitor.getInfo().activeMinutesWeek.total;
            	var dataCurrentCheck = ActivityMonitor.getInfo().activeMinutesWeek;
            	var dataIcon = activeIcon;
            //}
            
            
            var dataPercent = 1.0;
            if (null != dataCurrentCheck && null != dataGoal && dataCurrent <= dataGoal) {
                dataPercent = dataCurrent.toDouble()/ dataGoal;
            }
            dc.setColor(app.getProperty("accentColor"),Graphics.COLOR_BLACK);
            dc.setPenWidth(10);
            var aStart = 351;
            var len = 296;
            if (dataPercent >= 1.0) {
                len = 300;
            }
            var aEnd = (aStart + dataPercent * len).toLong()%360;
            if (aStart != aEnd) {
                dc.drawArc(lX,lY,sideInnerRadius,Graphics.ARC_COUNTER_CLOCKWISE, aStart, aEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            dc.drawText(lX, lY-4, tinyFont, dataCurrent , Graphics.TEXT_JUSTIFY_CENTER);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_TRANSPARENT);
            dc.drawText(lX, lY+10, tinyFont,dataGoal,  Graphics.TEXT_JUSTIFY_CENTER);
            if (null != dataIcon) {
                dc.drawBitmap(lX-10, lY-24, dataIcon);
            }
        }

        /* Floor Circle */
        if (floorIcon != null) {
            var rightAngle = (100.0 / 360) * Math.PI * 2;
            var rX = (width/2 + r * Math.sin(rightAngle)).toLong();
            var rY = (height/2 - r * Math.cos(rightAngle)).toLong();
            dc.setPenWidth(6);
            dc.setColor(app.getProperty("accentBackgroundColor"),Graphics.COLOR_BLACK);
            dc.drawArc(rX, rY,sideOuterRadius,Graphics.ARC_CLOCKWISE, 183, 258);

            var floorsClimbedGoal = ActivityMonitor.getInfo().floorsClimbedGoal;
            var floorsClimbed = ActivityMonitor.getInfo().floorsClimbed;
            var floorsPercent = 1.0;
            if (null != floorsClimbed && null != floorsClimbedGoal && floorsClimbed <= floorsClimbedGoal) {
                floorsPercent = floorsClimbed.toDouble()/floorsClimbedGoal;
            }
            dc.setColor(app.getProperty("accentColor"),Graphics.COLOR_BLACK);
            dc.setPenWidth(10);
            var fStart = 188;
            var len = 294;
            if (floorsPercent >= 1.0) {
                len = 300;
            }
            var fEnd = (fStart - floorsPercent * len).toLong()%360;
            if (fStart != fEnd) {
                dc.drawArc(rX,rY,sideInnerRadius,Graphics.ARC_CLOCKWISE, fStart, fEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            if (null != floorIcon) {
                dc.drawBitmap(rX-8, rY-24, floorIcon);
            }
            dc.drawText(rX, rY-4, tinyFont, floorsClimbed, Graphics.TEXT_JUSTIFY_CENTER);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_TRANSPARENT);
            dc.drawText(rX, rY+10, tinyFont, floorsClimbedGoal, Graphics.TEXT_JUSTIFY_CENTER);
        }

        /* Steps Cirle */
        if (stepsIcon) {
            dc.setPenWidth(6);
            dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_BLACK);
            dc.fillCircle(width/2, vCenter, stepOuterRadius);
            dc.setColor(app.getProperty("accentBackgroundColor"),Graphics.COLOR_BLACK);
            dc.drawCircle(width/2, vCenter, stepOuterRadius);

            var stepGoal = ActivityMonitor.getInfo().stepGoal;
            var steps = ActivityMonitor.getInfo().steps ;
            var stepsPercent = 1.0;
            if (null != steps && null != stepGoal && steps <= stepGoal) {
                stepsPercent = steps.toDouble()/stepGoal;
            }
            dc.setColor(app.getProperty("accentColor"),Graphics.COLOR_BLACK);
            dc.setPenWidth(10);
            var sStart = 90;
            var sEnd = (360 - (stepsPercent*360).toLong()+90)%360;
            if (sStart != sEnd || stepsPercent >= 1.0) {
                dc.drawArc(width/2,vCenter,stepInnerRadius,Graphics.ARC_CLOCKWISE, sStart, sEnd);
            }
            dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
            if (null != stepsIcon) {
                dc.drawBitmap((width/2)-12, vCenter-30, stepsIcon);
            }
            dc.drawText((width / 2), vCenter-4, tinyFont, steps, Graphics.TEXT_JUSTIFY_CENTER);
            dc.setColor(Graphics.COLOR_DK_GRAY,Graphics.COLOR_TRANSPARENT);
            dc.drawText((width / 2), vCenter+10, tinyFont, stepGoal, Graphics.TEXT_JUSTIFY_CENTER);            
        }
    }

    // Draws the clock tick marks around the outside edges of the screen.
    function drawHashMarks(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();

        // Draw hashmarks differently depending on screen geometry.
        if (System.SCREEN_SHAPE_ROUND == screenShape) {
            var sX, sY;
            var eX, eY;
            var outerRad = width / 2;
            var innerRad = outerRad - 16;
            var smallInnerRad = outerRad - 1;
            for (var j = 0; j < 60; j++) {
                var iRad = smallInnerRad;
                if (j%5==0) {
                    iRad = innerRad;
                    dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_LT_GRAY);
                    dc.setPenWidth(3);
                }
                var i = j * Math.PI / 30;
                sY = outerRad + iRad * Math.sin(i);
                eY = outerRad + outerRad * Math.sin(i);
                sX = outerRad + iRad * Math.cos(i);
                eX = outerRad + outerRad * Math.cos(i);
                dc.drawLine(sX, sY, eX, eY);
                if (j%5==0) {
                    dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_DK_GRAY);
                    dc.setPenWidth(1);
                }
            }
        } else {
            //Not supported
            System.println( "Only round watchfaces are supported!");
        }
    }

    // Handle the update event
    function onUpdate(dc) {
        var width;
        var height;
        var screenWidth = dc.getWidth();
        var clockTime = System.getClockTime();        
        var secondHand;
        var targetDc = null;
        
        // We always want to refresh the full screen when we get a regular onUpdate call.
        fullScreenRefresh = true;
        
        if(null != offscreenBuffer) {
            dc.clearClip();
            curClip = null;
            // If we have an offscreen buffer that we are using to draw the background,
            // set the draw context of that buffer as our target.
            targetDc = offscreenBuffer.getDc();
        } else {
            targetDc = dc;
        }

        width = targetDc.getWidth();
        height = targetDc.getHeight();

        // Fill the entire background with Black.
        targetDc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
        targetDc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());

        // Draw the tick marks around the edges of the screen
        drawHashMarks(targetDc);

        drawStatusBar(targetDc);

        drawTopField(targetDc);
        
        drawActivityCircles(targetDc);

        drawHands(targetDc);

        // Output the offscreen buffers to the main display if required.
        drawBackground(dc);
                        
        if( partialUpdatesAllowed ) {
            // If this device supports partial updates and they are currently
            // allowed run the onPartialUpdate method to draw the second hand.
            onPartialUpdate( dc );
        } else if ( isAwake ) {
            // Otherwise, if we are out of sleep mode, draw the second hand
            // directly in the full update method.
            if (Application.getApp().getProperty("showSeconds")){
            	dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
	            secondHand = (clockTime.sec / 60.0) * Math.PI * 2;

    	        dc.fillPolygon(generateHandCoordinates(screenCenterPoint, secondHand, 110, 20, 2));
    	    }
        }

        fullScreenRefresh = false;        
    }

    // Handle the partial update event
    function onPartialUpdate( dc ) {
        var width = dc.getWidth();
        var height = dc.getHeight();    
        // If we're not doing a full screen refresh we need to re-draw the background
        // before drawing the updated second hand position. Note this will only re-draw
        // the background in the area specified by the previously computed clipping region.
        if(!fullScreenRefresh) {
            drawBackground(dc);
        }

		if (!Application.getApp().getProperty("showSeconds")){
			return;
		}
        var clockTime = System.getClockTime();
        var secondHand = (clockTime.sec / 60.0) * Math.PI * 2;
        var secondHandPoints = generateHandCoordinates(screenCenterPoint, secondHand, 110, 20, 2);
        for (var i=6; i > 0; i--) {
        		dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
				dc.fillCircle(width /2, height /2, 6);
		}

        // Update the cliping rectangle to the new location of the second hand.
        curClip = getBoundingBox( secondHandPoints );
        var bboxWidth = curClip[1][0] - curClip[0][0] + 1;
        var bboxHeight = curClip[1][1] - curClip[0][1] + 1;
        dc.setClip(curClip[0][0], curClip[0][1], bboxWidth, bboxHeight);

        // Draw the second hand to the screen.
        dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
        dc.fillPolygon(secondHandPoints);
        
        // Draw the center hole
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
		dc.fillCircle(width /2, height /2, 3);
    }

    // Compute a bounding box from the passed in points
    function getBoundingBox( points ) {
        var min = [9999,9999];
        var max = [0,0];

        for (var i = 0; i < points.size(); ++i) {
            if(points[i][0] < min[0]) {
                min[0] = points[i][0];
            }

            if(points[i][1] < min[1]) {
                min[1] = points[i][1];
            }

            if(points[i][0] > max[0]) {
                max[0] = points[i][0];
            }

            if(points[i][1] > max[1]) {
                max[1] = points[i][1];
            }
        }

        return [min, max];
    }

    // Draw the watch face background
    // onUpdate uses this method to transfer newly rendered Buffered Bitmaps
    // to the main display.
    // onPartialUpdate uses this to blank the second hand from the previous
    // second before outputing the new one.
    function drawBackground(dc) {
        var width = dc.getWidth();
        var height = dc.getHeight();

        //If we have an offscreen buffer that has been written to
        //draw it to the screen.
        if( null != offscreenBuffer ) {
            dc.drawBitmap(0, 0, offscreenBuffer);
        }
    }

    // This method is called when the device re-enters sleep mode.
    // Set the isAwake flag to let onUpdate know it should stop rendering the second hand.
    function onEnterSleep() {
        isAwake = false;
        WatchUi.requestUpdate();
    }

    // This method is called when the device exits sleep mode.
    // Set the isAwake flag to let onUpdate know it should render the second hand.
    function onExitSleep() {
        isAwake = true;
    }
}

class AnalogDelegate extends WatchUi.WatchFaceDelegate {

    function initialize() {
        WatchFaceDelegate.initialize();
    }
    // The onPowerBudgetExceeded callback is called by the system if the
    // onPartialUpdate method exceeds the allowed power budget. If this occurs,
    // the system will stop invoking onPartialUpdate each second, so we set the
    // partialUpdatesAllowed flag here to let the rendering methods know they
    // should not be rendering a second hand.
    function onPowerBudgetExceeded(powerInfo) {
        System.println( "Average execution time: " + powerInfo.executionTimeAverage );
        System.println( "Allowed execution time: " + powerInfo.executionTimeLimit );
        partialUpdatesAllowed = false;
    }
}
